import java.util.Scanner;


public class ApplianceStore {
    public static void main(String[] args) {
        Fridge[] fridges = new Fridge[4];
        Scanner scanner = new Scanner(System.in);
        
        for (int i = 0; i < fridges.length; i++) {
            fridges[i] = new Fridge();
            System.out.println("Enter whether the fridge has milk (true/false): ");
            fridges[i].hasMilk = Boolean.parseBoolean(scanner.nextLine());
            System.out.println("Enter how many melons the fridge has: ");
            fridges[i].melonCount = Integer.parseInt(scanner.nextLine());
            System.out.println("Enter the cake flavour in the fridge: ");
            fridges[i].cakeFlavor = scanner.nextLine();
        }
        
        System.out.println("Does the last fridge have milk: " + fridges[fridges.length - 1].hasMilk + ", how many melons are there: " + fridges[fridges.length - 1].melonCount + " and has cake of flavour: " + fridges[fridges.length - 1].cakeFlavor);
        fridges[0].pourMilk(fridges[0].hasMilk);
        fridges[0].serveMelons(fridges[0].melonCount);
    }
}