public class Fridge {
    public boolean hasMilk; 
    public int melonCount;
    public String cakeFlavor; 
    
    public void pourMilk(boolean hasMilk) {
        if (hasMilk) {
            System.out.println("Milk is being poured!");
        } else {
            System.out.println("No milk left in the fridge!");
        }
    }
    
    public void serveMelons(int numMelons) {
        if (numMelons > melonCount) {
            System.out.println("Not enough melons!");
        } else {
            melonCount -= numMelons;
            System.out.println("Serving " + numMelons + " melons!");
        }
    }
}
